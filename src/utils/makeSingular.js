// Yes I needed this

/**
 * @description Get the singular version of a string.
 * Necessary because it is
 * @param {string} str
 */
export default function makeSingular(str) {
  if (str === 'policies') return 'policy';
  return str.slice(0, -1);
}
