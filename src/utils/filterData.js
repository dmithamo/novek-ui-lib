/**
 * @description Filter visible items onFilterSelection
 */
export function filterData(filters, rawData) {
  if (Object.entries(filters).length === 0) {
    // if no filters
    return rawData;
  }

  return rawData.filter((item) => {
    let matchesAllFilters = true;

    function matchesFilter(filter) {
      const [key, value] = filter;

      return item[`${key}`] && item[`${key}`].includes(value);
    }

    Object.entries(filters).forEach((flt) => {
      matchesAllFilters *= matchesFilter(flt); // This is quite clever :)
    });

    return matchesAllFilters;
  });
}

export function handleFilterSelection() {}
