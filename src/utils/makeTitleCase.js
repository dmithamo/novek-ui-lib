/**
 * @description Create the titleCase version of a str
 * @param {string} string
 */
export default function makeTitleCase(string) {
  if (!string || string === '') return string;

  let words = string.replace(/\//g, ' ').split(' ');
  words = words.map((w) => w[0].toUpperCase() + w.slice(1));

  return words.join(' ');
}
