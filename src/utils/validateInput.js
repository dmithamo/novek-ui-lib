/**
 * @description Validate all the things!
 */

const PASSWORD_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/;
/*
 * (?=.*\d)          should contain at least one digit
 * (?=.*[a-z])       should contain at least one lower case
 * (?=.*[A-Z])       should contain at least one upper case
 * [a-zA-Z0-9]{8,}   should contain at least 8 from the mentioned characters
 *
 * */

const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
/*
 *
 * */

const NUMBER_REGEX = /^[0-9]{8}$/;
/*
 * [0-9]    should contain only decimal numbers
 * {8}    should be of length === 8
 */

const STRING_REGEX = /^[a-zA-Z0-9_-]{3,}$/;
/*
 * [a-zA-Z0-9_-]    should contain only alphanumerics letters, underscore, dash
 * {3,}    should have length >= 3
 */

export function isValidPassword(password) {
  return PASSWORD_REGEX.test(password);
}

export function isValidEmail(email) {
  return EMAIL_REGEX.test(email);
}

export function isValidSerialNumber(number) {
  return NUMBER_REGEX.test(number);
}

export function isValidString(string) {
  return STRING_REGEX.test(string);
}
