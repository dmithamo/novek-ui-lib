/**
 * @description Remove critical fields from data
 * @param {object} data Object frm which to remove critical data
 * @param {array} criticalFields Fields to remove
 */
export function deleteCriticalFields(data, criticalFields) {
  return criticalFields.reduce((acc, field) => {
    delete acc[`${field}`];
    return acc;
  }, data);
}

/**
 * @description
 * @param {object} data Object in which to reassign boolean values
 * @param {array} reAssignTo Values to reAssign to
 */
export function reAssignBooleanValues(data, toReAssign) {
  function reAssign(item) {
    toReAssign.forEach((element) => {
      const [k, v] = Object.entries(element)[0];
      const { truthyValue, falsyValue } = v;
      const currentValue = item[`${k}`];

      // Reassign only if it's a boolean.
      if (typeof currentValue === 'boolean') {
        item[`${k}`] = currentValue ? truthyValue : falsyValue;
      }
    });

    return item;
  }

  return reAssign(data);
}
