import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function FullPageOverlay(props) {
  const { children } = props;
  return <FullPageContainer>{children}</FullPageContainer>;
}

FullPageOverlay.propTypes = {
  children: PropTypes.any.isRequired,
};

const FullPageContainer = styled.div`
  margin: 0;
  display: flex;
  justify-content: space-around;
  align-items: center;
  position: absolute;
  left: 0;
  top: 0;
  height: 100vh;
  width: 100%;
  background-color: #52525260;
  background-color: #00000098;
  z-index: 999;
`;

export function RightSideOverlay(props) {
  const { title, onClickExitBtn, children } = props;
  return (
    <RightSideContainer>
      <p>
        <button type="button" onClick={onClickExitBtn}>
          <FontAwesomeIcon icon="arrow-left" />
        </button>
        <span>{title}</span>
      </p>
      {children}
    </RightSideContainer>
  );
}

RightSideOverlay.propTypes = {
  children: PropTypes.any.isRequired,
  title: PropTypes.any.isRequired,
  onClickExitBtn: PropTypes.func.isRequired,
};

const RightSideContainer = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  z-index: 1000;
  margin: 0;
  padding: 1em;
  height: 100vh;
  width: 25%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  p {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 1.1em;

    button {
      outline: none;
      border: none;
      background: none;
      color: inherit;
      cursor: pointer;

      :hover {
        color: #000;
      }
    }

    span {
      margin: 0 1em;
      font-weight: bold;
    }
  }
  background-color: #ffffff;
`;
export function CenteredOverlay(props) {
  const { title, onClickExitBtn, children } = props;
  return (
    <CenteredOverlayContainer>
      <p id="title">
        <span>{title}</span>
        <button type="button" onClick={onClickExitBtn}>
          <FontAwesomeIcon icon="times" />
        </button>
      </p>
      <div id="main-content">{children}</div>
    </CenteredOverlayContainer>
  );
}

CenteredOverlay.propTypes = {
  children: PropTypes.any.isRequired,
  title: PropTypes.any.isRequired,
  onClickExitBtn: PropTypes.func.isRequired,
};

const CenteredOverlayContainer = styled.div`
  z-index: 1000;
  margin: 0;
  padding: 1.5em 5em;
  height: 90vh;
  width: 60%;
  display: grid;
  grid-template-rows: 1fr 10fr;
  background-color: #ffffff;
  box-shadow: 0 0 5px 1px #00000050;

  p#title {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 1.5em;
    margin: 0;
    padding: 1.2em 0;

    button {
      font-size: inherit;
      outline: none;
      border: none;
      background: none;
      color: inherit;
      cursor: pointer;
      font-weight: normal;

      :hover {
        color: red;
      }
    }

    span {
      font-weight: bold;
    }
  }

  div#main-content {
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
  }
`;
