import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * @description Render an inline error
 */
export function InlineError({ error }) {
  const errText = error.message;

  // TODO: Consider showing more info about error. e.g error.status
  return (
    <InlineErrorContainer>
      {error.status ? `::ERR_CODE ${error.status}::${errText}` : errText}
    </InlineErrorContainer>
  );
}

const InlineErrorContainer = styled.span`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;
  color: red;
`;

InlineError.propTypes = {
  error: PropTypes.any.isRequired,
};

export function ErrorAlert() {}
