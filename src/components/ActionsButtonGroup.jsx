import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import Button from './Button';

export default function ActionsButtonGroup(props) {
  const { actions } = props;

  return (
    <ButtonGroupContainer>
      {actions.map((action) => (
        <Button
          key={uuidv4()}
          id={uuidv4()}
          onClick={action.func}
          value={action.name}
          disabled={false}
        />
      ))}
    </ButtonGroupContainer>
  );
}

ActionsButtonGroup.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const ButtonGroupContainer = styled.span`
  width: 100%;
  padding: 0.5em;
  display: flex;
  justify-content: space-between;
  align-items: center;

  button {
    margin: 0 0.5em;
    padding: 0.5em 0.8em;
    font-weight: normal;
  }
`;
