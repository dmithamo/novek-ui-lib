import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default function InlineError(props) {
  const {
    error: { status, message },
  } = props;
  return (
    <InlineErrorContainer>{`ERR_CODE ${status}: ${message}`}</InlineErrorContainer>
  );
}

InlineError.propTypes = {
  error: PropTypes.any.isRequired,
};

const InlineErrorContainer = styled.header`
  color: #FF0000 !important;
  padding: 2em;
  font-weight: bold;
  font-size: 1.3em;
`;
