import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TopNavBar from './TopNavBar';

// For login screen only, really
export const FrontLayout = ({ children }) => (
  <FrontLayoutContainer>{children}</FrontLayoutContainer>
);

FrontLayout.propTypes = {
  children: PropTypes.any.isRequired,
};

const FrontLayoutContainer = styled.section`
  background-image: linear-gradient(to bottom right, #4e68a5, #081b48);
  width: 100%;
  min-height: 100vh;
  margin: 0;
  padding: 0;
  box-sizing: border-box;

  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

// Dashboard-esq pages
export const DashboardLayout = ({ children, context }) => (
  <DashboardLayoutContainer>
    <section id="main-content">
      <TopNavBar context={context} />
      <div id="layout-primary">{children}</div>
    </section>
  </DashboardLayoutContainer>
);

DashboardLayout.propTypes = {
  children: PropTypes.any.isRequired,
  context: PropTypes.any.isRequired,
};

const DashboardLayoutContainer = styled.div`
  * {
    box-sizing: border-box;
  }
  background-color: #00000008;
  margin: 0;
  width: 100%;
  display: grid;
  grid-template-columns: 15% 85%;

  nav#sidebar {
    height: auto;
  }

  section#main-content {
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    height: auto;
    margin: 0 auto;
    width: 98%;

    div#layout-primary {
      height: auto;
      width: 100%;
      flex-grow: 1;
      margin: 1.5em 0;
      border-radius: 5px;

      background-color: #fff;
    }
  }

  @media screen and (max-width: 1575px) {
    grid-template-columns: 5fr 95fr;
  }
`;
