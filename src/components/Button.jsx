import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default function Button(props) {
  const { id, onClick, value, disabled } = props;
  return (
    <ButtonContainer
      id={id}
      disabled={disabled}
      type="submit"
      onClick={onClick}
    >
      {value}
    </ButtonContainer>
  );
}

Button.propTypes = {
  id: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.any.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  id: '',
  disabled: false,
};

const ButtonContainer = styled.button`
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
  display: flex;
  justify-content: center;
  align-items: center;
  outline: none;
  color: ${(props) => (props.disabled ? '#000' : '#fff')};
  border: ${(props) => `2px solid ${props.disabled ? '#00000008' : '#010203'}`};
  border-radius: 3px;
  padding: 0.8em;
  text-transform: capitalize;
  width: 100%;
  font: inherit;
  font-weight: bold;
  font-size: 1em;
  transition: all 0.3s ease;
  background-color: ${(props) => !props.disabled && '#18579a'};
  box-sizing: border-box;

  :hover {
    background-color: ${(props) => !props.disabled && '#032d62'};
  }
`;
