import React from 'react';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const CustomLoader = ({ stateName }) => (
  <LoaderContainer>
    <Loader {...loaderProps} />
    <p>{`Loading ${stateName} ...`}</p>
  </LoaderContainer>
);

const LoaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  background: none;
  font-weight: bolder;
  color: black;
`;

const loaderProps = {
  type: 'TailSpin',
  color: 'black',
};

CustomLoader.propTypes = {
  stateName: PropTypes.string,
};

CustomLoader.defaultProps = {
  stateName: 'resource',
};

export default CustomLoader;
