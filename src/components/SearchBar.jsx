import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function SearchBar(props) {
  const { onSearchInput, searchQuery } = props;
  function onClickSearchIcon(e) {
    e.target.parentNode.nextElementSibling.focus();
  }

  return (
    <Container>
      <FontAwesomeIcon onClick={(e) => onClickSearchIcon(e)} icon="search" />
      <input
        value={searchQuery}
        onChange={onSearchInput}
        type="text"
        placeholder="Enter search term"
      />
    </Container>
  );
}

SearchBar.propTypes = {
  onSearchInput: PropTypes.func.isRequired,
  searchQuery: PropTypes.string.isRequired,
};

const Container = styled.p`
  width: 50%;
  margin: 0;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border: 1px solid #e3e3e3;
  border-radius: 5px;
  padding: 0.9em 2em;

  :focus-within {
    border: 1px solid #000;
  }

  svg {
    cursor: pointer;
    font-size: 1.2em;
  }

  input {
    font: inherit;
    border: none;
    outline: none;
    margin-left: 0.5em;
  }
`;
