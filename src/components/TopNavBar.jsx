import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import SearchBar from './SearchBar';
import DropdownMenu from './DropdownMenu';

export default function TopNavBar(props) {
  const { context } = props;
  return (
    <Container>
      <div className="left">
        <SearchBar onSearchInput={() => {}} searchQuery="" />
      </div>
      <div className="right">
        <DropdownMenu useAuthContext={context} />
      </div>
    </Container>
  );
}

TopNavBar.propTypes = {
  context: PropTypes.any.isRequired,
};

const Container = styled.div`
  width: 100%;
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.1em;
  background-color: #fff;

  div.right,
  div.left {
    margin: 0;
    display: flex;
    align-items: center;
    width: 50%;
  }

  div.left {
    justify-content: flex-start;
  }

  div.right {
    justify-content: flex-end;
  }

  * {
    box-sizing: border-box;
  }
`;
