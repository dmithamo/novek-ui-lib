import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default function Input(props) {
  const {
    label,
    type,
    name,
    value,
    placeholder,
    onChange,
    onBlur,
    required,
    children,
    max,
    min,
    step,
    error,
  } = props;

  return (
    <InputContainer>
      <label htmlFor={name}>
        {label}
        <input
          autoComplete="off"
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          onBlur={onBlur}
          required={required}
          // necessary for type=number
          max={max}
          min={min}
          step={step}
          error={error ? 1 : 0}
        />
        <span>{children}</span>
      </label>
    </InputContainer>
  );
}

Input.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  children: PropTypes.any,
  max: PropTypes.any,
  min: PropTypes.any,
  step: PropTypes.any,
  error: PropTypes.bool,
};

Input.defaultProps = {
  label: '',
  placeholder: '',
  onChange: () => {},
  onBlur: () => {},
  required: true,
  children: '',
  max: '',
  min: '',
  step: '',
  error: false,
};

const InputContainer = styled.div`
  margin: 0;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: flex-start;
  padding-bottom: 2em;

  label {
    font-weight: bolder;
    width: 100%;

    input {
      border: 1px solid #808080;
      border-color: ${(props) => (props.error ? 'red' : '#808080')};
      outline: none;
      width: 100%;
      border-radius: 4px;
      padding: 1.2em;
      font: inherit;
      margin: 5px 0;
      font-weight: normal;

      :focus {
        border: 1px solid #000;
      }
    }

    span {
      font-weight: normal;
    }
  }
`;
