import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { SelectDropdown } from './Select';

export default function Filters(props) {
  const { filtersObject, onFilterSelect } = props;
  const { label, options } = filtersObject;

  return (
    <FiltersContainer>
      <span className="label">{label}</span>

      {options
        && options.map((opt) => (
          <SelectDropdown
            key={Object.keys(opt)[0]}
            onChange={onFilterSelect}
            options={opt}
          />
        ))}
    </FiltersContainer>
  );
}

// TODO: Figure filters out

Filters.propTypes = {
  filtersObject: PropTypes.any.isRequired,
  onFilterSelect: PropTypes.func.isRequired,
};

const FiltersContainer = styled.div`
  background-color: #fff;
  width: 100%;
  padding: 1em;
  margin: 0;

  display: flex;
  justify-content: flex-start;
  align-items: center;

  span.label {
    color: #000;
    text-transform: capitalize;
    font-weight: bolder;
  }
`;
