import React from 'react';
import PropTypes from 'prop-types';
import { FullPageOverlay, CenteredOverlay } from './Overlay';

export default function FullPageErr({ children, onClose, title }) {
  return (
    <FullPageOverlay>
      <CenteredOverlay title={title} onClickExitBtn={onClose}>
        {children}
      </CenteredOverlay>
    </FullPageOverlay>
  );
}

FullPageErr.propTypes = {
  children: PropTypes.any.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.any.isRequired,
};
