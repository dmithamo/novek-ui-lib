import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Table from '../../Table';
import Filters from '../../Filters';
import Button from '../../Button';
import { InlineError } from '../../Error';
import SearchBar from '../../SearchBar';
import CustomLoader from '../../Loader';
import { ItemsManagementContainer } from './containerStyles';
import { deleteCriticalFields, reAssignBooleanValues } from '../../../utils/cleanUpData';
import { filterData } from '../../../utils/filterData';
import makeSingular from '../../../utils/makeSingular';

export default function ItemsManagement(props) {
  const {
    stateName,
    onRender,
    isFetching,
    fetchError,
    items,
    fieldsToHide,
    fieldsToReassign,
    NewItemForm,
    primaryFilters,
    secondaryFilters,
    showCreateNewMenu,
    path,
    toggleCreateNewMenu,
    showNewItemBtn,
  } = props;

  // CDM, CDU
  React.useEffect(() => {
    onRender();
  }, [onRender]);

  (() => {
    items.map((item) => deleteCriticalFields(item, fieldsToHide));
  })();

  (() => {
    items.map((item) => reAssignBooleanValues(item, fieldsToReassign));
  })();

  // Handle search
  const [searchQuery, setSearchQuery] = React.useState('');
  function handleSearchInput(e) {
    setSearchQuery(e.target.value);
  }

  // handle filter selection
  const [filters, setFilters] = React.useState({});
  function handleFilterSelect(selected) {
    const [key, value] = Object.entries(selected)[0];

    const unsupportedFilters = ['isArchived', 'date'];
    if (unsupportedFilters.includes(key)) {
      return;
      // Not supporting this. Yet
    }

    setFilters({
      ...filters,
      [key]: key === value ? '' : value,
    });
  }

  // RENDER STUFF!!
  if (isFetching) {
    return <CustomLoader stateName={stateName} />;
  }

  if (fetchError) {
    return <InlineError error={fetchError} stateName={stateName} />;
  }

  const filteredItems = filterData(filters, items);
  return (
    <ItemsManagementContainer>
      <section id="top">
        <div id="filters">
          {items.length > 0 && (
            <Fragment>
              <Filters filtersObject={primaryFilters} onFilterSelect={handleFilterSelect} />

              <Filters filtersObject={secondaryFilters} onFilterSelect={handleFilterSelect} />
            </Fragment>
          )}
        </div>

        <div id="search">
          {items.length > 0 && (
            <SearchBar onSearchInput={handleSearchInput} searchQuery={searchQuery} />
          )}
        </div>

        <div id="buttons">
          <Button id="export-items" onClick={() => {}} disabled={false} value="Export" />

          {showNewItemBtn && (
            <Button
              id={`create-new-${makeSingular(stateName)}`}
              onClick={toggleCreateNewMenu}
              disabled={false}
              value={`create new ${makeSingular(stateName)}`}
            />
          )}
        </div>
      </section>

      <div id="items-list">
        <Table stateName={stateName} data={filteredItems} />
      </div>

      {showCreateNewMenu && (
        <NewItemForm
          editingMode={false}
          path={path}
          stateName={stateName}
          onClickExitBtn={toggleCreateNewMenu}
        />
      )}
    </ItemsManagementContainer>
  );
}

ItemsManagement.propTypes = {
  stateName: PropTypes.string.isRequired,
  onRender: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchError: PropTypes.any.isRequired,
  fieldsToHide: PropTypes.any.isRequired,
  fieldsToReassign: PropTypes.any.isRequired,
  NewItemForm: PropTypes.any.isRequired,
  path: PropTypes.string.isRequired,
  primaryFilters: PropTypes.any.isRequired,
  secondaryFilters: PropTypes.any.isRequired,
  showNewItemBtn: PropTypes.bool,
  showCreateNewMenu: PropTypes.bool.isRequired,
  toggleCreateNewMenu: PropTypes.func.isRequired,
};

ItemsManagement.defaultProps = {
  showNewItemBtn: true,
};
