import styled from 'styled-components';

export const NewItemFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  input {
    padding: 0.8em;
    border-color: #00000050;

    :focus {
      border-color: #00000060;
    }
  }

  div#buttons {
    display: flex;
    width: 60%;
    justify-content: space-between;

    button {
      width: 45%;
    }

    button#cancel {
      background-color: #fff;
      border: 1px solid #000;
      color: #000;
      filter: opacity(0.7);

      :hover {
        filter: opacity(1);
      }
    }
  }
`;

export const ItemsManagementContainer = styled.div`
  margin: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;
  width: 100%;
  padding: 0.7em;

  section#top {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    margin: 2em 0;

    div {
      margin: 0 auto;
      width: 80%;
    }

    div#filters {
      div {
        width: 100%;
      }
    }

    div#search {
      display: flex;
      justify-content: space-around;
      align-items: center;
    }

    div#buttons {
      display: flex;
      justify-content: space-around;
      width: 45%;
      align-items: center;

      button {
        width: 45%;
        margin: 0;
      }
    }
  }

  div#items-list {
    margin: 0;
    margin-top: 1em;
    width: 100%;
  }
`;
