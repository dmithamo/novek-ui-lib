import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { InlineError } from '../../Error';
import CustomLoader from '../../Loader';
import Button from '../../Button';
import { deleteCriticalFields, reAssignBooleanValues } from '../../../utils/cleanUpData';

export default function SingleItemManagement(props) {
  const {
    onRender,
    item,
    fieldsToHide,
    fieldsToReassign,
    isFetching,
    fetchError,
    stateName,
    actions,
    EditForm,
    path,
    toggleEditForm,
    showEditForm,
  } = props;

  React.useEffect(() => {
    onRender();
  }, [onRender]);

  (() => {
    deleteCriticalFields(item, fieldsToHide);
  })();

  (() => {
    reAssignBooleanValues(item, fieldsToReassign);
  })();

  if (isFetching) {
    return <CustomLoader stateName={stateName} />;
  }

  if (fetchError) {
    return <InlineError error={fetchError} stateName={stateName} />;
  }

  if (showEditForm) {
    return <EditForm editingMode path={path} />;
  }
  return (
    <SingleItemContainer>
      <div id="item">
        {Object.entries(item).map((p) => (
          <ParamContainer key={JSON.stringify(p)} entry={p} />
        ))}
      </div>

      <div id="buttons">
        {actions.map((act) => (
          <Button
            key={JSON.stringify(act)}
            onClick={actions.indexOf(act) === 0 ? toggleEditForm : act.func}
            value={act.name}
          />
        ))}
      </div>
    </SingleItemContainer>
  );
}

SingleItemManagement.propTypes = {
  item: PropTypes.any.isRequired,
  isFetching: PropTypes.bool.isRequired,
  fetchError: PropTypes.any.isRequired,
  stateName: PropTypes.string.isRequired,
  path: PropTypes.any.isRequired,
  onRender: PropTypes.func.isRequired,
  actions: PropTypes.array.isRequired,
  EditForm: PropTypes.any.isRequired,
  toggleEditForm: PropTypes.func.isRequired,
  showEditForm: PropTypes.bool.isRequired,
  fieldsToHide: PropTypes.any,
  fieldsToReassign: PropTypes.any,
};

SingleItemManagement.defaultProps = {
  fieldsToHide: [],
  fieldsToReassign: [],
};

const SingleItemContainer = styled.div`
  margin: 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  box-shadow: 0 0 5px 2px #00000001;
  padding: 1em 0;

  div {
    margin: 0;
  }

  div#item {
    border: 1px solid #e3e3e3;
    display: grid;
    grid-template-columns: 1fr 1fr;
  }

  div#buttons {
    button {
      margin-right: 0.5em;
    }
    width: 90%;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
  }

  @media screen and (min-width: 720px) {
    width: 90%;
  }

  @media screen and (min-width: 1024px) {
    width: 100%;
  }
`;

// ---------------------------------------------------------------

export function ParamContainer(props) {
  const {
    entry: [key, value],
  } = props;

  return (
    <StyledParamContainer>
      <span>{`${key}:`}</span>
      <span>
        {value && typeof value === 'object'
          ? Object.entries(value).map((p) => <ParamContainer key={JSON.stringify(p)} entry={p} />)
          : `${value}`}
      </span>
    </StyledParamContainer>
  );
}

ParamContainer.propTypes = {
  entry: PropTypes.any.isRequired,
};

const StyledParamContainer = styled.div`
  display: grid;
  grid-template-columns: 100px 1fr;
  padding: 0.2em 1.1em;
  border: 1px solid #dfdfdf;
  margin: 0;

  span:first-of-type {
    opacity: 0.9;
    font-weight: bold;
    text-transform: capitalize;
  }
  span:last-of-type {
  }
`;
