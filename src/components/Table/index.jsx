/* eslint-disable no-unused-vars */
import React, { Fragment } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

import ActionsButtonGroup from '../ActionsButtonGroup';
import Pagination from './Pagination';

export default function Table(props) {
  const { stateName } = props;
  let { data } = props;
  const totalNumberOfItems = data.length;

  // Embed numbers, and extract actions
  data = data.map((item) => {
    const itemNumber = data.indexOf(item) + 1;
    return { '#': `${itemNumber}.`, ...item };
  });

  /**
   * @description Extract tableHeaders from the data
   */
  function getTableHeaders() {
    return Object.keys(data[0]).filter((h) => h !== 'actions');
  }

  /**
   * @description Generate the body of the table given its headers
   * and data
   * @param {object} row
   */
  function getTableBody(row) {
    return getTableHeaders()
      .filter((h) => h !== 'actions')
      .map((header) => {
        let contents = row[header];
        if (!!contents && typeof contents === 'object') {
          const firstKey = Object.keys(contents)[0];
          contents = contents[firstKey];
        }

        return <TableDataCell key={uuidv4()} contents={contents} />;
      });
  }

  const [itemsPerPage, setItemsPerPage] = React.useState(
    totalNumberOfItems > 10 ? 10 : totalNumberOfItems,
  );
  let numberOfPages = Math.ceil(totalNumberOfItems / itemsPerPage);
  numberOfPages = isNaN(numberOfPages) || !isFinite(numberOfPages) ? 1 : numberOfPages;

  function handleSetItemsPerPage(e) {
    const { value } = e.target;

    // Value must never exceed totalNumberOfItems
    const valueToSet = value > totalNumberOfItems ? totalNumberOfItems : value;

    setItemsPerPage(valueToSet);

    // Reset to first page
    // TODO: this is a short circuit for a very interesting bug. Handle later
    setPageNumber(1);
  }

  const [currentPageNumber, setPageNumber] = React.useState(1);
  function handleNavBtnClick(pageNum) {
    if (pageNum === 'NEXT') {
      handleNextBtnClick();
      return;
    }
    if (pageNum === 'PREV') {
      handlePrevBtnClick();
      return;
    }

    setPageNumber(pageNum);
  }
  function handleNextBtnClick() {
    setPageNumber(currentPageNumber === numberOfPages ? currentPageNumber : currentPageNumber + 1);
  }
  function handlePrevBtnClick() {
    setPageNumber(currentPageNumber === 1 ? 1 : currentPageNumber - 1);
  }

  // Slice the array to paginate the data
  let startIndex = currentPageNumber === 1 ? 0 : (currentPageNumber - 1) * itemsPerPage;
  startIndex = startIndex > totalNumberOfItems ? totalNumberOfItems : startIndex;

  let endIndex = itemsPerPage * currentPageNumber;
  endIndex = endIndex > totalNumberOfItems ? totalNumberOfItems : endIndex;

  const paginatedData = data.slice(startIndex, endIndex);

  const paddingRows = 10 - paginatedData.length;
  /**
   * @description Pad the table with empty rows to give it a standard height
   * @param {Number} number
   */
  function embedPaddingRows(number) {
    const paddingRow = (key) => (
      <tr className="padding-row" key={key}>
        {[...getTableHeaders(), 'actions'].map((_) => (
          <TableDataCell key={uuidv4()} contents="." />
        ))}
      </tr>
    );

    return (
      <Fragment key="item">{new Array(number).fill('').map((__) => paddingRow(uuidv4()))}</Fragment>
    );
  }

  // RENDER ALL THE THINGS!!
  if (totalNumberOfItems === 0) {
    return <p>{`No ${stateName} to display`}</p>;
  }
  return (
    <Fragment key="table">
      <StyledTable>
        <thead>
          <tr>
            {getTableHeaders().map((header) => (
              <td key={uuidv4()}>{header}</td>
            ))}
            <td>Actions</td>
          </tr>
        </thead>
        <tbody>
          {paginatedData.map((row) => (
            <tr key={uuidv4()}>
              <Fragment key="header-row">{getTableBody(row)}</Fragment>
              <td>
                <ActionsButtonGroup actions={row.actions} />
              </td>
            </tr>
          ))}
          {paddingRows > 0 && embedPaddingRows(paddingRows)}
        </tbody>
      </StyledTable>
      <Pagination
        totalItems={totalNumberOfItems}
        itemsPerPage={itemsPerPage}
        stateName={stateName}
        handleSetItemsPerPage={handleSetItemsPerPage}
        handleNavBtnClick={handleNavBtnClick}
        currentPageNumber={currentPageNumber}
        numberOfPages={numberOfPages}
        showingFrom={startIndex}
        showingTo={endIndex}
      />
    </Fragment>
  );
}

const TableDataCell = (props) => {
  const { contents } = props;
  return <StyledTableDataCell contents={String(contents)}>{String(contents)}</StyledTableDataCell>;
};

Table.propTypes = {
  stateName: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

TableDataCell.propTypes = {
  contents: PropTypes.any,
};

TableDataCell.defaultProps = {
  contents: null,
};

const StyledTable = styled.table`
  width: 100%;
  white-space: nowrap;
  border-collapse: collapse;
  box-shadow: 0 0 2px 1px #00000010;

  table,
  thead,
  tbody,
  tr {
    border: 1px solid #838383;
  }

  thead {
    font-weight: bolder;
    color: #00008b;
    text-transform: capitalize;

    td {
      text-align: start;

      :last-of-type {
        text-align: center;
      }
    }
  }

  tbody {
    tr {
      :nth-of-type(odd) {
        background: #e3e3e3;
      }

      td {
        text-align: start;
      }
    }
  }

  tr {
    border: 1px solid #e3e3e3;
  }

  tr.padding-row {
    td {
      opacity: 0.01;
    }
  }

  thead,
  tbody {
    td {
      height: 50px !important;

      :first-of-type {
        text-align: center;
        padding: 0 0.3em;
        font-weight: bold;
      }
    }
  }
`;

const toStyleRed = ['offline', 'temporary'];
const toStyleGreen = ['online', 'permanent'];
const StyledTableDataCell = styled.td`
  color: ${(props) => toStyleRed.includes(props.contents) && 'red'};
  color: ${(props) => toStyleGreen.includes(props.contents) && 'green'};
  text-align: start;
`;
