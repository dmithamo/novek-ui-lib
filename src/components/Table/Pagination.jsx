import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import Input from '../Input';

export default function Pagination(props) {
  const {
    totalItems,
    itemsPerPage,
    stateName,
    handleSetItemsPerPage,
    currentPageNumber,
    numberOfPages,
    handleNavBtnClick,
    showingFrom,
    showingTo,
  } = props;

  function getShowingCaption() {
    return `Showing ${showingFrom + 1} to ${showingTo} of ${totalItems} entries`;
  }

  function getPageBtns() {
    const btnsArray = new Array(numberOfPages)
      .fill('')
      .slice(0, 10)
      .map((_, index) => ({ pageNum: index + 1 }));

    return btnsArray;
  }

  return (
    <PaginationContainer>
      <Input
        onChange={handleSetItemsPerPage}
        label="Show"
        value={itemsPerPage}
        type="number"
        name="pagination"
        stateName={stateName}
        max="100"
        min="1"
        step="3"
      >
        Entries
      </Input>
      <div id="nav">
        {getShowingCaption()}
        <NavButtonsContainer>
          <NavButton
            onClick={() => {
              handleNavBtnClick('PREV');
            }}
            disabled={Number(currentPageNumber) === 1}
            pageNum="Previous"
          />

          {getPageBtns().map((btn) => (
            <NavButton
              key={uuidv4()}
              onClick={() => {
                handleNavBtnClick(btn.pageNum);
              }}
              pageNum={btn.pageNum}
              isActive={currentPageNumber === btn.pageNum}
            />
          ))}

          <NavButton
            onClick={() => {
              handleNavBtnClick('NEXT');
            }}
            disabled={Number(currentPageNumber) === numberOfPages}
            pageNum="Next"
          />
        </NavButtonsContainer>
      </div>
    </PaginationContainer>
  );
}

Pagination.propTypes = {
  totalItems: PropTypes.any.isRequired,
  itemsPerPage: PropTypes.any.isRequired,
  stateName: PropTypes.string.isRequired,
  handleSetItemsPerPage: PropTypes.func.isRequired,
  handleNavBtnClick: PropTypes.func.isRequired,
  currentPageNumber: PropTypes.any.isRequired,
  numberOfPages: PropTypes.any.isRequired,
  showingFrom: PropTypes.any.isRequired,
  showingTo: PropTypes.any.isRequired,
};

const PaginationContainer = styled.div`
  font-size: 0.9em;
  opacity: 0.9;
  border: 1px solid #e3e3e3;
  padding: 1.2em;
  box-shadow: 0 0 2px 1px #00000010;
  margin: 0;
  width: 100%;

  display: flex;
  justify-content: space-between;
  align-items: center;

  div {
    margin: 0;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    padding: 0;
  }

  div > label {
    margin: 0;
    display: flex;
    align-items: center;
    flex-direction: row;

    input {
      width: 50px;
      padding: 1em 0.5em;
      margin: 0 0.5em;
    }
  }

  div#nav {
    display: flex;
    justify-content: flex-end;
    width: 100%;
  }
`;

const NavButton = (props) => {
  const { onClick, pageNum, isActive, disabled } = props;
  return (
    <StyledNavButton disabled={disabled} isActive={isActive} onClick={onClick} type="button">
      {pageNum}
    </StyledNavButton>
  );
};

NavButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  pageNum: PropTypes.any.isRequired,
  isActive: PropTypes.bool,
  disabled: PropTypes.bool,
};
NavButton.defaultProps = {
  isActive: false,
  disabled: false,
};

const StyledNavButton = styled.button`
  padding: 0.5em 1em;
  background-color: ${(props) => (props.isActive ? '#032d62' : '#A8A8A8')};
  color: ${(props) => (props.isActive ? '#fff' : 'black')};
  border: 1px solid #e3e3e3;
  border-radius: 2px;
  font-weight: normal;
  outline: none;
  margin: 0 0.2em;
  font: inherit;
  font-weight: bold;
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
`;

const NavButtonsContainer = styled.span`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;
