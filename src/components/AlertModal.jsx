import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from './Button';
import { FullPageOverlay } from './Overlay';

export default function ConfirmActionModal(props) {
  const {
    textContent, onConfirm, confirmBtnText, onCancel, isPosting,
  } = props;

  return (
    <FullPageOverlay>
      <ConfirmActionModalContainer>
        <p>{textContent}</p>
        <ButtonsContainer>
          {onCancel && (
            <Button
              id="cancel-btn"
              onClick={onCancel}
              disabled={isPosting}
              value="Cancel"
            />
          )}

          {onConfirm && (
            <Button
              id="confirm-btn"
              onClick={onConfirm}
              disabled={isPosting}
              value={confirmBtnText || 'confirm'}
            />
          )}
        </ButtonsContainer>
      </ConfirmActionModalContainer>
    </FullPageOverlay>
  );
}

ConfirmActionModal.defaultProps = {
  onCancel: false,
  onConfirm: false,
  confirmBtnText: false,
  isPosting: false,
};

ConfirmActionModal.propTypes = {
  textContent: PropTypes.any.isRequired,
  isPosting: PropTypes.bool,
  onConfirm: PropTypes.any,
  onCancel: PropTypes.any,
  confirmBtnText: PropTypes.any,
};

const ConfirmActionModalContainer = styled.div`
  padding: 3em 2em;
  width: 25%;
  height: 20vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0 0 4px 1px #00000060;
  background-color: #fff;
  border-radius: 5px;

  p {
    padding: 0;
    margin: 1em 0;
    font-weight: bold;
  }
`;

const ButtonsContainer = styled.span`
  width: 70%;
  display: flex;
  justify-content: space-around;
  align-items: center;

  button {
    width: 40%;
    font: inherit;
  }

  button#cancel-btn {
    margin-left: 2em;
    padding: 0.66em;
    background: none;
    border: 2px solid #00000040;
    color: #000;
  }
`;
