import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

export function SelectDropdown(props) {
  const { label, options, selected, onChange } = props;

  const [optionsLabel, optionsList] = Object.entries(options)[0];
  /**
   * @description Handle changes in the select
   * @param {event} e
   */
  function handleSelectChange(e) {
    onChange({
      [e.target.name]: e.target.value,
      target: { value: e.target.value },
      // Temp W/around. Need to simplify this :(
    });
  }

  return (
    <SelectContainer>
      <label htmlFor={optionsLabel}>
        {label && label}
        <select value={selected} name={optionsLabel} onChange={(e) => handleSelectChange(e)}>
          <option value="">{`--${optionsLabel}--`}</option>
          {optionsList.map((opt) => (
            <option key={`${opt.value || opt}`} value={`${opt.value || opt}`}>
              {`${opt.name || opt}`}
            </option>
          ))}
        </select>
      </label>
    </SelectContainer>
  );
}

SelectDropdown.propTypes = {
  label: PropTypes.any,
  options: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  selected: PropTypes.any,
};

SelectDropdown.defaultProps = {
  label: null,
  selected: '',
};

export function SelectDate(props) {
  const { limit, label } = props;
  return (
    <SelectContainer>
      <label htmlFor="date">
        {label}
        <input name="date" type="date" max={limit} />
      </label>
    </SelectContainer>
  );
}

SelectDate.propTypes = {
  limit: PropTypes.any.isRequired,
  label: PropTypes.string.isRequired,
};

const SelectContainer = styled.span`
  margin: 0;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: flex-start;
  padding-bottom: 2em;

  label {
    font-weight: bolder;
    width: 100%;

    select,
    input {
      background: white;
      border: 1px solid #808080;
      border-color: ${(props) => (props.error ? 'red' : '#808080')};
      outline: none;
      width: 100%;
      border-radius: 4px;
      padding: 1.1em;
      font: inherit;
      margin: 5px 0;
      font-weight: normal;

      :focus {
        border: 1px solid #000;
      }
    }

    select {
      text-transform: capitalize;
    }
  }
`;

export function CheckBox(props) {
  const { name, value, checked, onChange } = props;

  return (
    <RadioButtonContainer>
      <label htmlFor={name}>
        <input
          checked={checked && checked.includes(value)}
          name={name}
          type="checkbox"
          onChange={onChange}
          value={value}
        />
        {`${value}`}
      </label>
    </RadioButtonContainer>
  );
}

CheckBox.propTypes = {
  name: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  checked: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

export function CheckBoxGroup(props) {
  const { label, name, options, checked, onChange } = props;

  return (
    <RadioButtonGroupContainer numberOfItems={options.length}>
      <label htmlFor={name}>
        {label}
        <div id="options">
          {options.map((opt) => (
            <CheckBox key={opt} checked={checked} onChange={onChange} name={name} value={opt} />
          ))}
        </div>
      </label>
    </RadioButtonGroupContainer>
  );
}

CheckBoxGroup.propTypes = {
  label: PropTypes.any.isRequired,
  name: PropTypes.any.isRequired,
  options: PropTypes.any.isRequired,
  checked: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

export function RadioButton(props) {
  const { name, value, checked, onChange } = props;
  return (
    <RadioButtonContainer>
      <label htmlFor="id">
        <input
          checked={value === checked}
          name={name}
          type="radio"
          onChange={onChange}
          value={value}
        />
        {`${value}`}
      </label>
    </RadioButtonContainer>
  );
}

RadioButton.propTypes = {
  name: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  checked: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

const RadioButtonContainer = styled.span`
  label {
    display: grid;
    grid-template-columns: 1fr 8fr;
    grid-gap: 1em;
    padding: 2em;
    text-transform: capitalize;
  }
`;

export function RadioButtonGroup(props) {
  const { label, name, options, checked, onChange } = props;

  return (
    <RadioButtonGroupContainer numberOfItems={options.length}>
      <label htmlFor={name}>
        {label}
        <div id="options">
          {options.map((opt) => (
            <RadioButton key={opt} checked={checked} onChange={onChange} name={name} value={opt} />
          ))}
        </div>
      </label>
    </RadioButtonGroupContainer>
  );
}

RadioButtonGroup.propTypes = {
  label: PropTypes.any.isRequired,
  name: PropTypes.any.isRequired,
  options: PropTypes.any.isRequired,
  checked: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

const RadioButtonGroupContainer = styled.div`
  label {
    font-weight: bold;
  }

  div#options {
    display: grid;
    grid-template-columns: ${(props) => (props.numberOfItems > 4 ? '1fr 1fr' : '1fr')};
  }
`;
