import React, { useState } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

export default function DropdownMenu(props) {
  const { useAuthContext } = props;
  const [showHiddenItems, setShowHiddenItems] = useState(false);

  function toggleDropdown() {
    setShowHiddenItems(!showHiddenItems);
  }

  const auth = useAuthContext();
  const {
    authState: {
      user: { username, avatar },
    },
  } = auth;

  return (
    <DropdownMenuContainer>
      <DropdownMenuItem id="user-icon" onClick={toggleDropdown}>
        <img src={avatar} alt="user icon" />
        <span>{username}</span>
      </DropdownMenuItem>

      <FontAwesomeIcon
        id="dropdown-toggle"
        onClick={toggleDropdown}
        icon={showHiddenItems ? 'chevron-up' : 'chevron-down'}
      />

      {showHiddenItems && (
        <HiddenItemsContainer>
          <DropdownMenuItem id="logout-btn" onClick={auth.onLogout}>
            <FontAwesomeIcon icon="sign-out-alt" />
            <span>Logout</span>
          </DropdownMenuItem>
        </HiddenItemsContainer>
      )}
    </DropdownMenuContainer>
  );
}

DropdownMenu.propTypes = {
  useAuthContext: PropTypes.any.isRequired,
};

const DropdownMenuContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 25%;

  #dropdown-toggle {
    cursor: pointer;
  }

  @media screen and (max-width: 1575px) {
    width: 30%;
  }
`;

const DropdownMenuItem = styled.p`
  margin: 0;
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 70%;

  img {
    width: 35px;
    height: 35px;
    border-radius: 50%;
  }

  :hover {
    color: #000;
  }
`;

const HiddenItemsContainer = styled.div`
  width: 200px;
  flex-direction: column;
  padding: 1.2em;
  position: absolute;
  top: 5vh;
  right: 1%;
  background-color: #fff;
  box-shadow: 1px 1px 5px 2px #00000050;
`;
